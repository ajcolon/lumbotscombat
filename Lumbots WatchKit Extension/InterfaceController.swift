//
//  InterfaceController.swift
//  Lumbots WatchKit Extension
//
//  Created by Albith Joel Colon on 12/3/14.
//  Copyright (c) 2014 Albith Joel Colon. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    //let appDelegate:AppDelegate!

    override init(context: AnyObject?) {
        // Initialize variables here.
        super.init(context: context)
        
        //Load App delegate?
        //appDelegate = UIApplication.sharedApplication().delegate as AppDelegate

        
        // Configure interface objects here.
        NSLog("%@ init", self)
    }
    @IBAction func clickOnAction1() {
        println("clickOnAction1");
    }
    
    @IBAction func clickOnAction2() {
        println("clickOnAction2");
        
    }
    @IBAction func clickOnAction3() {
        println("clickOnAction3");
    }
    
    func sendData(data: NSDictionary){
        let messageData = NSJSONSerialization.dataWithJSONObject(data, options: NSJSONWritingOptions.PrettyPrinted, error: nil);
        var error:NSError?
        //appDelegate.mpcHandler.session.sendData(messageData, toPeers: appDelegate.mpcHandler.session.connectedPeers, withMode: MCSessionSendDataMode.Reliable, error: &error)
        
        if error != nil{
            println("error: \(error?.localizedDescription)")
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        NSLog("%@ will activate", self)
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        NSLog("%@ did deactivate", self)
        super.didDeactivate()
    }

}
