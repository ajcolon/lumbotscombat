//
//  LumbotScene.swift
//  Lumbots
//
//  Created by Albith Joel Colon on 12/4/14.
//  Copyright (c) 2014 Albith Joel Colon. All rights reserved.
//
import SpriteKit
import MultipeerConnectivity

class BattleScene : SKScene {

    var appDelegate:AppDelegate!
    var lumbot:Lumbot?
    var battleControl:UIBattleControl!;
    
    //Abilities Buttons
    var abitlity1:LumbotAction?
    var abitlity2:LumbotAction?
    var abitlity3:LumbotAction?
    
    //ViewController Reference
    var vController: GameViewController?;
    
    //AbilityContainer
    let abilityContainer = SKSpriteNode(color: UIColor.redColor(), size: CGSizeMake(100,100));
    
    //Ability Labels
    let abilityEnergy1 = SKLabelNode(fontNamed:"Futura-Medium")
    let abilityAmount1 = SKLabelNode(fontNamed:"Futura-Medium")
    let abilityType1 = SKLabelNode(fontNamed:"Futura-Medium")
    let abilityEnergy2 = SKLabelNode(fontNamed:"Futura-Medium")
    let abilityAmount2 = SKLabelNode(fontNamed:"Futura-Medium")
    let abilityType2 = SKLabelNode(fontNamed:"Futura-Medium")
    let abilityEnergy3 = SKLabelNode(fontNamed:"Futura-Medium")
    let abilityAmount3 = SKLabelNode(fontNamed:"Futura-Medium")
    let abilityType3 = SKLabelNode(fontNamed:"Futura-Medium")
    
    //Ability BG's
    let ability1AttackBG = SKSpriteNode(imageNamed: "AttackIcon");
    let ability1EnergyBG = SKSpriteNode(imageNamed: "EnergyIcon");
    let ability2AttackBG = SKSpriteNode(imageNamed: "AttackIcon");
    let ability2EnergyBG = SKSpriteNode(imageNamed: "EnergyIcon");
    let ability3AttackBG = SKSpriteNode(imageNamed: "AttackIcon");
    let ability3EnergyBG = SKSpriteNode(imageNamed: "EnergyIcon");
    
    //Labels
    let myHealthLabel = SKLabelNode(fontNamed:"Futura-Medium")
    let myLevelLabel = SKLabelNode(fontNamed:"Futura-Medium")
    let turnLabel = SKLabelNode(fontNamed:"Futura-Medium")
    let energyLabel = SKLabelNode(fontNamed:"Futura-Medium")
    
    //localvars
    var myPeerId: MCPeerID!;
    var opponentPeerId: MCPeerID!;
    var currentPlayer: MCPeerID!;
    var currentTurn:Int = 0;
    var isConnected:Bool = false;
    
    //Messages Type
    enum MessageType: Int{
        case ability = 0
        case endgame = 1
        case endturn = 2
        case disconnect = 3
    }
    
    override func didMoveToView(view: SKView) {
        appDelegate = UIApplication.sharedApplication().delegate as AppDelegate

        self.setUIElements();
        self.setHelmetActionsOnUI();
        self.setNotificationObservers();
    }
    
    func setNotificationObservers(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "peerChangedStateWithNotification:", name: "MPC_DidChangeStateNotification", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleReceivedDataWithNotification:", name: "MPC_DidReceiveDataNotification", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleReceivedHelmetWithNotification:", name: "Lumbots_DidChangedHelmet", object: nil)
    
    }
    
    func setAbilityContainerHidden(isHidden: Bool){
        self.abilityContainer.hidden = isHidden;
    }
    
    func setHelmetActionsOnUI(){
        
        let isDefaultHidden: Bool = true;
        
        //Add AbilityContainer
        self.abilityContainer.size = CGSizeMake(self.frame.width, self.frame.height/2);
        self.abilityContainer.position = CGPointMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame)+5);
        self.addChild(self.abilityContainer);
        
        
        //add ability1 button
        self.abitlity1 = LumbotAction(imageNamed: "abuttonAttack");
        self.abitlity1!.position = CGPointMake(CGRectGetMidX(self.frame)-80, CGRectGetMinY(self.frame)+40);
        self.abitlity1!.xScale = 0.12
        self.abitlity1!.yScale = 0.12
        self.abitlity1?.hidden = isDefaultHidden;
        self.abilityContainer.addChild(self.abitlity1!);
        
        //Set Labels Ability 1
        self.abilityEnergy1.text = "0";
        self.abilityEnergy1.fontSize = 9;
        self.abilityEnergy1.hidden = isDefaultHidden;
        self.abilityEnergy1.position = CGPointMake(CGRectGetMidX(self.frame)-60, CGRectGetMinY(self.frame)+5);
        self.abilityContainer.addChild(self.abilityEnergy1);
        
        //Set Labels Ability 1
        self.abilityAmount1.text = "0";
        self.abilityAmount1.fontSize = 9;
        self.abilityAmount1.hidden = isDefaultHidden;
        self.abilityAmount1.position = CGPointMake(CGRectGetMidX(self.frame)-85, CGRectGetMinY(self.frame)+5);
        self.abilityContainer.addChild(self.abilityAmount1);
        
        //Add LabelsBG
        self.ability1AttackBG.position = CGPointMake(CGRectGetMidX(self.frame)-90, CGRectGetMinY(self.frame)+10);
        self.ability1AttackBG.xScale = 0.036
        self.ability1AttackBG.yScale = 0.04
        self.ability1AttackBG.hidden = isDefaultHidden;
        self.abilityContainer.addChild(self.ability1AttackBG);
        self.ability1EnergyBG.position = CGPointMake(CGRectGetMidX(self.frame)-65, CGRectGetMinY(self.frame)+10);
        self.ability1EnergyBG.xScale = 0.05
        self.ability1EnergyBG.yScale = 0.05
        self.ability1EnergyBG.hidden = isDefaultHidden;
        self.abilityContainer.addChild(self.ability1EnergyBG);
        
        
        //Set ability button 2
        self.abitlity2 = LumbotAction(imageNamed: "abuttonAttack");
        self.abitlity2!.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMinY(self.frame)+40);
        self.abitlity2!.xScale = 0.12
        self.abitlity2!.yScale = 0.12
        self.abitlity2?.hidden = isDefaultHidden;
        self.abilityContainer.addChild(self.abitlity2!);
        
        //Set Labels Ability 2
        self.abilityEnergy2.text = "0";
        self.abilityEnergy2.fontSize = 9;
        self.abilityEnergy2.hidden = isDefaultHidden;
        self.abilityEnergy2.position = CGPointMake(CGRectGetMidX(self.frame)+15, CGRectGetMinY(self.frame)+5);
        self.abilityContainer.addChild(self.abilityEnergy2);
        
        //Set Labels Ability 2
        self.abilityAmount2.text = "0";
        self.abilityAmount2.fontSize = 9;
        self.abilityAmount2.hidden = isDefaultHidden;
        self.abilityAmount2.position = CGPointMake(CGRectGetMidX(self.frame)-11, CGRectGetMinY(self.frame)+5);
        self.abilityContainer.addChild(self.abilityAmount2);
        
        //Add LabelsBG
        self.ability2AttackBG.position = CGPointMake(CGRectGetMidX(self.frame)-12, CGRectGetMinY(self.frame)+10);
        self.ability2AttackBG.xScale = 0.036
        self.ability2AttackBG.yScale = 0.04
        self.ability2AttackBG.hidden = isDefaultHidden;
        self.abilityContainer.addChild(self.ability2AttackBG);
        self.ability2EnergyBG.position = CGPointMake(CGRectGetMidX(self.frame)+12, CGRectGetMinY(self.frame)+10);
        self.ability2EnergyBG.xScale = 0.05
        self.ability2EnergyBG.yScale = 0.05
        self.ability2EnergyBG.hidden = isDefaultHidden;
        self.abilityContainer.addChild(self.ability2EnergyBG);
        
        //add ability3 button
        self.abitlity3 = LumbotAction(imageNamed: "abuttonAttack");
        self.abitlity3!.position = CGPointMake(CGRectGetMidX(self.frame)+80, CGRectGetMinY(self.frame)+40);
        self.abitlity3!.xScale = 0.12
        self.abitlity3!.yScale = 0.12
        self.abitlity3?.hidden = isDefaultHidden;
        self.abilityContainer.addChild(self.abitlity3!);
        
        //Set Labels Ability 1
        self.abilityEnergy3.text = "0";
        self.abilityEnergy3.fontSize = 9;
        self.abilityEnergy3.hidden = isDefaultHidden;
        self.abilityEnergy3.position = CGPointMake(CGRectGetMidX(self.frame)+95, CGRectGetMinY(self.frame)+5);
        self.abilityContainer.addChild(self.abilityEnergy3);
        
        //Set Labels Ability 1
        self.abilityAmount3.text = "0";
        self.abilityAmount3.fontSize = 9;
        self.abilityAmount3.hidden = isDefaultHidden;
        self.abilityAmount3.position = CGPointMake(CGRectGetMidX(self.frame)+73, CGRectGetMinY(self.frame)+5);
        self.abilityContainer.addChild(self.abilityAmount3);
        
        //Add LabelsBG
        self.ability3AttackBG.position = CGPointMake(CGRectGetMidX(self.frame)+70, CGRectGetMinY(self.frame)+10);
        self.ability3AttackBG.xScale = 0.036
        self.ability3AttackBG.yScale = 0.04
        self.ability3AttackBG.hidden = isDefaultHidden;
        self.abilityContainer.addChild(self.ability3AttackBG);
        self.ability3EnergyBG.position = CGPointMake(CGRectGetMidX(self.frame)+92, CGRectGetMinY(self.frame)+10);
        self.ability3EnergyBG.xScale = 0.05
        self.ability3EnergyBG.yScale = 0.05
        self.ability3EnergyBG.hidden = isDefaultHidden;
        self.abilityContainer.addChild(self.ability3EnergyBG);
    }
    
    func setHelmetActions(isHidden:Bool){
        
        //let isHidden: Bool = true;
        self.abitlity1?.hidden = isHidden;
        self.abitlity2?.hidden = isHidden;
        self.abitlity3?.hidden = isHidden;
        self.abilityEnergy1.hidden = isHidden;
        self.abilityAmount1.hidden = isHidden;
        self.abilityEnergy2.hidden = isHidden;
        self.abilityAmount2.hidden = isHidden;
        self.abilityEnergy3.hidden = isHidden;
        self.abilityAmount3.hidden = isHidden;
        self.ability1AttackBG.hidden = isHidden;
        self.ability1EnergyBG.hidden = isHidden;
        self.ability2AttackBG.hidden = isHidden;
        self.ability2EnergyBG.hidden = isHidden;
        self.ability3AttackBG.hidden = isHidden;
        self.ability3EnergyBG.hidden = isHidden;
        
        
        var abilities = self.lumbot?.helmet?.abilities;
        if(abilities?.count > 0){
            
            if(abilities?.count == 1){
                var helmAbility1 = abilities![0] as Ability;
                self.abitlity2?.texture = SKTexture(imageNamed: helmAbility1.imagePath);
                self.abitlity2?.hidden = false;
                self.abilityEnergy2.hidden = false;
                self.abilityAmount2.hidden = false;
                self.abilityEnergy2.text = " \(helmAbility1.reqEnergy!)";
                self.abilityAmount2.text = " \(helmAbility1.ammount!)";
                self.ability2AttackBG.hidden = false;
                self.ability2EnergyBG.hidden = false;
                
            }else if(abilities?.count == 2){
                //HelmAbility 1
                var helmAbility1 = abilities![0] as Ability;
                self.abitlity1?.texture = SKTexture(imageNamed: helmAbility1.imagePath);
                self.abitlity1?.hidden = false;
                self.abilityEnergy1.hidden = false;
                self.abilityAmount1.hidden = false;
                self.abilityEnergy1.text = " \(helmAbility1.reqEnergy!)";
                self.abilityAmount1.text = " \(helmAbility1.ammount!)";
                self.ability1AttackBG.hidden = false;
                self.ability1EnergyBG.hidden = false;
                
                //HelmAbility 2
                var helmAbility2 = abilities![1] as Ability;
                self.abitlity3?.texture = SKTexture(imageNamed: helmAbility2.imagePath);
                self.abitlity3?.hidden = false;
                self.abilityEnergy3.hidden = false;
                self.abilityAmount3.hidden = false;
                self.abilityEnergy3.text = " \(helmAbility2.reqEnergy!)";
                self.abilityAmount3.text = " \(helmAbility2.ammount!)";
                self.ability3AttackBG.hidden = false;
                self.ability3EnergyBG.hidden = false;

            }else if(abilities?.count == 3 ){
                //Helm Ability 1
                var helmAbility1 = abilities![0] as Ability;
                self.abitlity1?.texture = SKTexture(imageNamed: helmAbility1.imagePath);
                self.abitlity1?.hidden = false;
                self.abilityEnergy1.hidden = false;
                self.abilityAmount1.hidden = false;
                self.abilityEnergy1.text = " \(helmAbility1.reqEnergy!)";
                self.abilityAmount1.text = " \(helmAbility1.ammount!)";
                self.ability1AttackBG.hidden = false;
                self.ability1EnergyBG.hidden = false;
                
                //Helm Ability 2
                var helmAbility2 = abilities![1] as Ability;
                self.abitlity2?.texture = SKTexture(imageNamed: helmAbility2.imagePath);
                self.abitlity2?.hidden = false;
                self.abilityEnergy2.hidden = false;
                self.abilityAmount2.hidden = false;
                self.abilityEnergy2.text = " \(helmAbility2.reqEnergy!)";
                self.abilityAmount2.text = " \(helmAbility2.ammount!)";
                self.ability2AttackBG.hidden = false;
                self.ability2EnergyBG.hidden = false;
                
                //Helm Ability 3
                var helmAbility3 = abilities![2] as Ability;
                self.abitlity3?.texture = SKTexture(imageNamed: helmAbility3.imagePath);
                self.abitlity3?.hidden = false;
                self.abilityEnergy3.hidden = false;
                self.abilityAmount3.hidden = false;
                self.abilityEnergy3.text = " \(helmAbility3.reqEnergy!)";
                self.abilityAmount3.text = " \(helmAbility3.ammount!)";
                self.ability3AttackBG.hidden = false;
                self.ability3EnergyBG.hidden = false;
            }else{
                println("Error in abilitites");
            }
        }else{
            println("Helmet doesnt have abilities");
        }
    }
    
    func setUIElements(){
        
        //Background
        let backgroundImage = SKSpriteNode(imageNamed: "Background0001");
        backgroundImage.xScale = 0.5
        backgroundImage.yScale = 0.5
        backgroundImage.size.height = self.frame.height;
        backgroundImage.size.width = self.frame.width;
        backgroundImage.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame));
        self.addChild(backgroundImage);
        
        //Append lumbot
        self.lumbot = Lumbot(imageNamed: "Lumbot");
        self.lumbot!.xScale = 0.5
        self.lumbot!.yScale = 0.5
        self.lumbot!.lumbotBaseline = CGRectGetMidY(self.frame);
        self.lumbot!.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame));
        self.lumbot?.idleState();
        self.addChild(self.lumbot!)
        
        //Add Turn label
        self.turnLabel.text = "";
        self.turnLabel.fontSize = 12;
        self.turnLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMinY(self.frame)+120);
        self.addChild(self.turnLabel);
        
        //Add Battle Control
        self.battleControl = UIBattleControl();
        self.battleControl!.xScale = 0.5
        self.battleControl!.yScale = 0.5
        self.battleControl!.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMinY(self.frame)+100);
        self.battleControl.updateDisplay(self.lumbot!);
        self.addChild(self.battleControl!)
        
        //change background color
        self.backgroundColor = UIColor(hex:0x1E2028);
    }
    
    func setLumbotHealth(health:Int){
        self.lumbot?.setHealth(health);
        self.myHealthLabel.text = String(self.lumbot!.getHealth());
    }

 
    func resetGame(){
        println("resetting game");
        self.lumbot?.health = 100;
        self.lumbot?.energy = 0;
    }
    
    func displayCurrentPlayer(){
        println("displaying current player turn: \(self.currentPlayer!.displayName)");
        //self.turnLabel.text = "\(self.currentPlayer!.displayName) Turn";
        if(currentPlayer == myPeerId){
            //self.turnLabel.text = "End Turn";
            self.battleControl.displayMyTurn();
        }else{
            //self.turnLabel.text = "\(self.currentPlayer!.displayName) Turn";
            self.battleControl.displayOpponentTurn();
        }
    }
    
    func setCurrentTurnEnergy(){
        currentTurn++;
        if(currentTurn >= 5){
            self.lumbot?.energy = 5;
        }else{
           self.lumbot?.energy = currentTurn;
            
        }
        
        println("Current Turn: \(self.currentTurn) and Current Player Energy \(self.lumbot?.energy)");
        self.battleControl.updateDisplay(self.lumbot!);
    }
    
//    func displayCurrentEnergy(){
//        self.energyLabel.text = "\(self.lumbot!.energy) / 5";
//    }
    
    func togglePlayerTurn(){
        if(currentPlayer === myPeerId){
            currentPlayer = opponentPeerId;
        }else{
            currentPlayer = myPeerId;
        }
        self.displayCurrentPlayer();
    }
    
    func sendData(data: NSDictionary){
        
        let messageData = NSJSONSerialization.dataWithJSONObject(data, options: NSJSONWritingOptions.PrettyPrinted, error: nil);
        var error:NSError?
        appDelegate.mpcHandler.session.sendData(messageData, toPeers: appDelegate.mpcHandler.session.connectedPeers, withMode: MCSessionSendDataMode.Reliable, error: &error)
        
        if error != nil{
            println("error: \(error?.localizedDescription)")
        }
    }
    
    func showGameOverMessage(message:String){
        let alert = UIAlertController(title: "Game Over", message: "\(message)", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))

        self.vController?.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    func checkWinner(){
        println("checkWinner");
        if(self.lumbot?.health <= 0){
            let messageDict = ["messageType":MessageType.endgame.rawValue, "message":"Game over you win"];
            sendData(messageDict);
            showGameOverMessage("Game over you lose");
            resetGame();
        }
    }
    
    func showInvalidTurnMessage(){
        println("showInvalidTurnMessage");
        let alert = UIAlertController(title: "Opponent Turn", message: "Wait for the opponent to finish his turn", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        self.vController?.navigationController?.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func peerChangedStateWithNotification(notification:NSNotification){
        println("peerChangedStateWithNotification");
        
        let userInfo = NSDictionary(dictionary: notification.userInfo!)
        
        let state = userInfo.objectForKey("state") as Int
        println( "state: \(state)");
        
        if state == MCSessionState.Connected.rawValue{
            println("Connection Established");
            self.myPeerId = appDelegate.mpcHandler.peerID;
            println("My Peer Id:\(self.myPeerId)");
            self.opponentPeerId = userInfo["peerID"] as MCPeerID
            println("Opponent Peer Id:\(opponentPeerId)");
            
            if(currentPlayer == nil){
                currentPlayer = opponentPeerId;
            }
            self.isConnected = true;
            self.resetGame();
            self.setAbilityContainerHidden(false);
            self.displayCurrentPlayer();
            self.battleControl.show();
            self.battleControl.updateDisplay(self.lumbot!);
            
        }else if( state == MCSessionState.Connecting.rawValue){
            println("Connecting..");
            self.isConnected = false;
        }else if(state == MCSessionState.NotConnected.rawValue){
            self.isConnected = false;
            println("Disconnected..");
             self.battleControl.hide();
            self.setAbilityContainerHidden(true);
            self.showAlert("Player Disconnected", msg: "Please re-establish connection with opponent");
        }else{
            self.isConnected = false;
            println("Got to some weird connection state");
        }
        
    }
    
    func showAlert( title: NSString, msg: NSString){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        self.vController?.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    func toggleUIElementsVisibility(show:Bool){
        
        
    }
    
    func handleReceivedHelmetWithNotification(notification:NSNotification){
        let userInfo = notification.userInfo! as Dictionary
        var receivedHelmet = userInfo["helmet"] as Helmet
        println("handleReceivedHelmetWithNotification \(receivedHelmet)");
        //var helmet = Helmet(imageNamed: receivedHelmet.imagePath);
        self.lumbot?.setHelmet(receivedHelmet);
        self.setHelmetActions(self.isConnected);
    }
    
    func handleReceivedAbility(abilityObj:NSDictionary){
        if let typeRaw = abilityObj["type"] as AnyObject? as? Int {
            if let reqEnergy = abilityObj["reqEnergy"] as AnyObject? as? Int {
                if let ammount = abilityObj["ammount"] as AnyObject? as? Int {
                    let type = AbilityManager.AbilityType(rawValue: typeRaw);
                    let ability = AbilityManager().getAbilityByType(type!, ammount: ammount, energy: reqEnergy);
                    ability.applyReceiver(self.lumbot!);
                    self.battleControl.updateDisplay(self.lumbot!);
                }
            }
        }
    }
    
    func handleReceivedDataWithNotification(notification:NSNotification){
        println("handleReceivedDataWithNotification");
        let userInfo = notification.userInfo! as Dictionary
        let receivedData:NSData = userInfo["data"] as NSData
        
        let message = NSJSONSerialization.JSONObjectWithData(receivedData, options: NSJSONReadingOptions.AllowFragments, error: nil) as NSDictionary
        let senderPeerId:MCPeerID = userInfo["peerID"] as MCPeerID
        let senderDisplayName = senderPeerId.displayName
        
        var msgType:Int? = message.objectForKey("messageType")?.integerValue;
        println("Message type: \(msgType)");
        
        switch msgType!{
        case MessageType.ability.rawValue:
            var abilityObj:NSDictionary? = (message.objectForKey("ability") as NSDictionary);
            self.handleReceivedAbility(abilityObj!);
            checkWinner()
        case MessageType.endgame.rawValue:
            var description:String? = message.objectForKey("message") as? String
            showGameOverMessage(description!);
            resetGame();
        case MessageType.endturn.rawValue:
            self.setCurrentTurnEnergy();
            self.togglePlayerTurn();
        default:
            println("Error handling received notification");
            
        }
        
    }

    func isMyTurn() ->Bool{
        if(currentPlayer == myPeerId){
            return true;
        }else{
            showInvalidTurnMessage();
            return false;
        }
    }
    
    func showNotEnoughEnergyMsg(){
        let alert = UIAlertController(title: "Not enough energy", message: "Ability requires more energy than currently available", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        self.vController?.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    func testLumbotClick(){
        let tempAbility = AbilityManager().getAbilityByType(AbilityManager.AbilityType.clash,ammount: 20,energy:1);
        tempAbility.applySender(self.lumbot!);
        
        self.lumbot?.setHealth(-5);
        
        println("Lumbot Energy: \(self.lumbot?.energy)");
        if(self.lumbot?.energy == 0){
            self.lumbot?.energy = 5;
        }
        
        self.battleControl.updateDisplay(self.lumbot!);
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            
            let location = touch.locationInNode(self);
            if(self.nodeAtPoint(location) == self.abitlity1){
                self.ability1();
            }
            if(self.nodeAtPoint(location) == self.abitlity2 && currentPlayer == myPeerId){
                self.ability2();
            }
            if(self.nodeAtPoint(location) == self.abitlity3 && currentPlayer == myPeerId){
                self.ability3();
            }
            if(self.nodeAtPoint(location).name == "turnButton"  && currentPlayer == myPeerId){
                println("TouchTurnButton");
                self.endMyTurn();
            }
            
            if(self.nodeAtPoint(location) == self.lumbot){
                self.testLumbotClick();
            }
        }
        
        if(self.lumbot!.onGround){
            self.lumbot!.velocityY = -12.0;
            self.lumbot!.onGround = false;
        }
        
    }
    
    func endMyTurn(){
        if(self.isMyTurn()){
            self.togglePlayerTurn();
            let messageDict = ["messageType":MessageType.endturn.rawValue];
            sendData(messageDict);
        }
    }
    
    func handleClickedAbility(ability: Ability){
        if(self.lumbot!.hasEnoughEnergy(ability.reqEnergy!)){
            
            let messageDict = ["messageType":MessageType.ability.rawValue,"ability":ability.objectRepresentation()];
            self.sendData(messageDict);
            //self.handleSelfAbility(ability);
            self.lumbot?.handleAbility(ability);
            self.battleControl.updateDisplay(self.lumbot!);
        }else{
            self.showNotEnoughEnergyMsg();
        }
    }
    
    func ability1(){
        var abilities =  self.lumbot?.helmet?.abilities;
        if(self.isMyTurn()){
            var ability = abilities![0] as Ability;
            self.handleClickedAbility(ability);
        }
    }
    func ability2(){
        var abilities =  self.lumbot?.helmet?.abilities;
        if(self.isMyTurn()){
            var ability = abilities![abilities!.count-1] as Ability;
            self.handleClickedAbility(ability);
        }
        
    }
    func ability3(){
        var abilities =  self.lumbot?.helmet?.abilities;
        if(self.isMyTurn()){
            var ability = abilities![abilities!.count-1] as Ability;
            self.handleClickedAbility(ability);
        }
    }
    
    
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        if(self.lumbot!.velocityY < -6.0){
            self.lumbot!.velocityY = -6.0;
        }
        //self.lumbot?.toggleStates();
        
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
        
        self.lumbot?.update(currentTime);
        
        
    }
}
