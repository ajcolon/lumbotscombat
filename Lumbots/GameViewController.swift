//
//  ViewController.swift
//  Lumbots
//
//  Created by Albith Joel Colon on 12/1/14.
//  Copyright (c) 2014 Albith Joel Colon. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import SpriteKit;

extension SKNode {
    class func unarchiveFromFile(file : NSString) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            var sceneData = NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe, error: nil)!
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as BattleScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}

class GameViewController: UIViewController, MCBrowserViewControllerDelegate {
    
    var appDelegate:AppDelegate!
    var battleScene:BattleScene?
    var isConnected:Bool = false;
    
    @IBOutlet weak var connectBtn: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        appDelegate.mpcHandler.setupPeerWithDisplayName(UIDevice.currentDevice().name)
        appDelegate.mpcHandler.setupSession()
        appDelegate.mpcHandler.advertiseSelf(true)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "peerChangedStateWithNotification:", name: "MPC_DidChangeStateNotification", object: nil)
        
        println("Extracting scene")
        //LumbotCombatScene.unarchiveFromFile("")
        if let scene = BattleScene.unarchiveFromFile("BattleScene") as? BattleScene {
            
            //Set SKView
            let skView = self.view as SKView
            skView.showsFPS = false
            skView.showsNodeCount = false
            skView.ignoresSiblingOrder = true
            
            //Set BattleScene
            self.battleScene = scene;
            self.battleScene?.vController = self;
            self.battleScene?.size = skView.bounds.size;
            self.battleScene?.scaleMode = .AspectFill
            
            skView.presentScene(self.battleScene?)
        }
        
    }
    @IBAction func toggleConnection(sender: AnyObject) {
        if appDelegate.mpcHandler.session != nil && !self.isConnected{
            appDelegate.mpcHandler.setupBrowser()
            appDelegate.mpcHandler.browser.delegate = self
            self.battleScene?.currentPlayer = appDelegate.mpcHandler.peerID;
            self.presentViewController(appDelegate.mpcHandler.browser, animated: true, completion: nil);
            
        }else{
            println("User has clicked on disconnect");
            appDelegate.mpcHandler.disconnectPlayer();
        }
    }
    
    func peerChangedStateWithNotification(notification:NSNotification){
        println("peerChangedStateWithNotification");
        let userInfo = NSDictionary(dictionary: notification.userInfo!)
        
        let state = userInfo.objectForKey("state") as Int
        
        if state == MCSessionState.Connected.rawValue{
            println("Connection Established on GameViewController");
            self.mapButton.hidden = true;
            self.isConnected = true;
            self.connectBtn.imageView?.image = UIImage(named:"DisconnectIconTop");
        }else if (state == MCSessionState.NotConnected.rawValue){
            println("Connecting..");
            self.isConnected = false;
            self.connectBtn.imageView?.image = UIImage(named:"ConnectIconTop");
            self.mapButton.hidden = false;
            self.battleScene!.toggleUIElementsVisibility(false);
        }else{
            println("Connecting");
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController!) {
        appDelegate.mpcHandler.browser.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController!) {
        appDelegate.mpcHandler.browser.dismissViewControllerAnimated(true, completion: nil)
    }
    


}

