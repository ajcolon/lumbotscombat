//
//  Helmet.swift
//  Lumbots
//
//  Created by Albith Joel Colon on 12/4/14.
//  Copyright (c) 2014 Albith Joel Colon. All rights reserved.
//

import Foundation
import SpriteKit


class Helmet:SKSpriteNode{
    
    var imagePath:String = "";
    var abilities:[Ability] = [];
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(imageNamed: String){
        self.imagePath = imageNamed;
        let imageTexture = SKTexture(imageNamed: imageNamed)
        super.init(texture:imageTexture, color:nil, size: imageTexture.size());
    }
    
    func changeImage(imageNamed:String){
        self.imagePath = imageNamed;
        self.texture = SKTexture(imageNamed: imageNamed);
    }

    
}