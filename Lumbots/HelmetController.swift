//
//  HelmetController.swift
//  Lumbots
//
//  Created by Albith Joel Colon on 12/7/14.
//  Copyright (c) 2014 Albith Joel Colon. All rights reserved.
//

import Foundation
import UIKit;
class HelmetController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet var tableView: UITableView!;
    var appDelegate:AppDelegate!
    var helmetsList : [Helmet] = []
    override func viewDidLoad(){
        super.viewDidLoad()
        self.loadHelmets();
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
        self.tableView.separatorColor = UIColor.clearColor();
        
    }
    
    func loadHelmets(){
        
        var abilityManager = AbilityManager();
        
        var helmet1 = Helmet(imageNamed: "00001");
        var ability = abilityManager.getAbilityByType(AbilityManager.AbilityType.clash, ammount: 10, energy: 1);
        var ability2 = abilityManager.getAbilityByType(AbilityManager.AbilityType.attack, ammount: 15, energy: 2);
        helmet1.abilities.append(ability);
        helmet1.abilities.append(ability2);
        self.helmetsList.append(helmet1);
        
        var helmet2 = Helmet(imageNamed: "00002");
        ability = abilityManager.getAbilityByType(AbilityManager.AbilityType.attack, ammount: 13, energy: 2);
        helmet2.abilities.append(ability);
        self.helmetsList.append(helmet2);
        
        var helmet3 = Helmet(imageNamed: "00003");
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.attack, ammount: 5, energy: 1);
        helmet3.abilities.append(ability);
        self.helmetsList.append(helmet3);
        
        var helmet4 = Helmet(imageNamed: "00004");
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.attack, ammount: 15, energy: 2);
        helmet4.abilities.append(ability);
        self.helmetsList.append(helmet4);
        
        var helmet5 = Helmet(imageNamed: "00005");
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.attack, ammount: 10, energy: 2);
        helmet5.abilities.append(ability);
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.attack, ammount: 2, energy: 3);
        helmet5.abilities.append(ability);
        self.helmetsList.append(helmet5);
        
        var helmet6 = Helmet(imageNamed: "00006");
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.attack, ammount: 40, energy: 5);
        helmet6.abilities.append(ability);
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.clash, ammount: 40, energy: 5);
        helmet6.abilities.append(ability);
        self.helmetsList.append(helmet6);
        
        var helmet7 = Helmet(imageNamed: "00007");
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.heal, ammount: 10, energy: 4);
        helmet7.abilities.append(ability);
        self.helmetsList.append(helmet7);
        
        var helmet8 = Helmet(imageNamed: "00008");
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.spell, ammount: 30, energy: 5);
        helmet8.abilities.append(ability);
        self.helmetsList.append(helmet8);
        
        var helmet9 = Helmet(imageNamed: "00009");
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.spell, ammount: 10, energy: 1);
        helmet9.abilities.append(ability);
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.attack , ammount: 40, energy: 5);
        helmet9.abilities.append(ability);
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.clash, ammount: 40, energy: 4);
        helmet9.abilities.append(ability);
        self.helmetsList.append(helmet9);
        
        var helmet10 = Helmet(imageNamed: "00010");
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.spell, ammount: 1, energy: 0);
        helmet10.abilities.append(ability);
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.attack , ammount: 5, energy: 1);
        helmet10.abilities.append(ability);
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.clash, ammount: 20, energy: 1);
        self.helmetsList.append(helmet10);
        
        var helmet11 = Helmet(imageNamed: "00011");
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.clash, ammount: 4, energy: 0);
        helmet11.abilities.append(ability);
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.clash , ammount: 80, energy: 5);
        helmet11.abilities.append(ability);
        ability = abilityManager.getAbilityByType( AbilityManager.AbilityType.clash, ammount: 20, energy: 2);
        self.helmetsList.append(helmet11);
    }
    
    @IBAction func closeDialog(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func displayAbility(ability:Ability,abilityNum:Int, cell:HelmetTableViewCell){
        switch abilityNum{
        case 1:
            cell.ability1Image.image = UIImage(named: ability.imagePath);
            cell.ability1Amount.text = "\(ability.ammount!)";
            cell.ability1Energy.text = "\(ability.reqEnergy!)";
            cell.ability1Image.hidden = false;
            cell.ability1Amount.hidden = false;
            cell.ability1Energy.hidden = false;
            cell.ability1AttackIcon.hidden = false;
            cell.ability1EnergyIcon.hidden = false;
        case 2:
            cell.ability2Image.image = UIImage(named: ability.imagePath);
            cell.ability2Amount.text = "\(ability.ammount!)";
            cell.ability2Energy.text = "\(ability.reqEnergy!)";
            cell.ability2Image.hidden = false;
            cell.ability2Amount.hidden = false;
            cell.ability2Energy.hidden = false;
            cell.ability2AttackIcon.hidden = false;
            cell.ability2EnergyIcon.hidden = false;
        case 3:
            cell.ability3Image.image = UIImage(named: ability.imagePath);
            cell.ability3Amount.text = "\(ability.ammount!)";
            cell.ability3Energy.text = "\(ability.reqEnergy!)";
            cell.ability3Image.hidden = false;
            cell.ability3Amount.hidden = false;
            cell.ability3Energy.hidden = false;
            cell.ability3AttackIcon.hidden = false;
            cell.ability3EnergyIcon.hidden = false;
        default:
            println("no cell ability");
        }
        
    }
    
    func hideAllCellAbilities(cell: HelmetTableViewCell){
        cell.ability1Image.hidden = true;
        cell.ability1Amount.hidden = true;
        cell.ability1Energy.hidden = true;
        cell.ability1AttackIcon.hidden = true;
        cell.ability1EnergyIcon.hidden = true;
        cell.ability2Image.hidden = true;
        cell.ability2Amount.hidden = true;
        cell.ability2Energy.hidden = true;
        cell.ability2AttackIcon.hidden = true;
        cell.ability2EnergyIcon.hidden = true;
        cell.ability3Image.hidden = true;
        cell.ability3Amount.hidden = true;
        cell.ability3Energy.hidden = true;
        cell.ability3AttackIcon.hidden = true;
        cell.ability3EnergyIcon.hidden = true;
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCellWithIdentifier("HelmetCell", forIndexPath: indexPath) as UITableViewCell;
        var cell: HelmetTableViewCell = tableView.dequeueReusableCellWithIdentifier("HelmetCell", forIndexPath: indexPath) as HelmetTableViewCell;
        
        let selectedHelmet = self.helmetsList[indexPath.row];
        cell.helmetImage.image = UIImage(named: selectedHelmet.imagePath);
        let abilities = selectedHelmet.abilities;
        
        
        self.hideAllCellAbilities(cell);
        //Ability 1
        if(abilities.count == 1){
            self.displayAbility(abilities[0], abilityNum: 1, cell: cell);
        }
        //Ability 2
        if(abilities.count == 2){
            self.displayAbility(abilities[0], abilityNum: 1, cell: cell);
            self.displayAbility(abilities[1], abilityNum: 2, cell: cell);
        }
        //Ability 3
        if(abilities.count == 3){
            self.displayAbility(abilities[0], abilityNum: 1, cell: cell);
            self.displayAbility(abilities[1], abilityNum: 2, cell: cell);
            self.displayAbility(abilities[2], abilityNum: 3, cell: cell);
        }
        
        //cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width, 0, 0);
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, CGRectGetWidth(tableView.bounds));
        
        return cell;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.helmetsList.count;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("Selected Helmet");
        let selectedHelmet = self.helmetsList[indexPath.row];
        let selectedHelmetObject = ["helmet":selectedHelmet];
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName("Lumbots_DidChangedHelmet", object: nil, userInfo: selectedHelmetObject);
        })
        
        self.dismissViewControllerAnimated(true, completion: nil);
        
    }
    
}