//
//  AbilityManager.swift
//  Lumbots
//
//  Created by Albith Joel Colon Figueroa on 1/4/15.
//  Copyright (c) 2015 Albith Joel Colon. All rights reserved.
//

import Foundation

class AbilityManager:NSObject{
    
    enum AbilityType: Int{
        case attack = 0
        case heal = 1
        case clash = 2
        case spell = 3
    }
    
    func getAbilityByType(type:AbilityType, ammount:Int, energy:Int) ->Ability{
        
        var tempAbility:Ability?;
        
        switch (type){
        case AbilityType.clash:
            tempAbility = ClashAbility(type: type, ammount: ammount,energy: energy);
        case AbilityType.attack:
            tempAbility = ClashAbility(type: type, ammount: ammount,energy: energy);
        case AbilityType.heal:
            tempAbility = HealAbility(type: type, ammount: ammount,energy: energy);
        case AbilityType.spell:
            tempAbility = SpellAbility(type: type, ammount: ammount,energy: energy);
        default:
            println("Not Registered Ability Class");
            
        }
        return tempAbility!;
        
    }
}