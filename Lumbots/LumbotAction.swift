//
//  LumbotAction.swift
//  Lumbots
//
//  Created by Albith Joel Colon on 12/4/14.
//  Copyright (c) 2014 Albith Joel Colon. All rights reserved.
//

import Foundation
import SpriteKit


class LumbotAction:SKSpriteNode{
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(imageNamed: String){
        let imageTexture = SKTexture(imageNamed: imageNamed)
        super.init(texture:imageTexture, color:nil, size: imageTexture.size());
    }
    
    
}