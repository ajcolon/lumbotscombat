//
//  SpellAbility.swift
//  Lumbots
//
//  Created by Albith Joel Colon Figueroa on 1/3/15.
//  Copyright (c) 2015 Albith Joel Colon. All rights reserved.
//

import Foundation
import SpriteKit

class SpellAbility:Ability{
    
    override init(type: AbilityManager.AbilityType, ammount: Int, energy: Int) {
        super.init(type: type, ammount: ammount, energy: energy);
        self.imagePath = "abuttonSpell";
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func applySender(sender:Lumbot){
        super.applySender(sender);
        let userPosX = sender.position.x;
        let spellActionFront = SKAction.moveToX(userPosX + 10.0, duration: 0.01);
        spellActionFront.timingMode = SKActionTimingMode.EaseOut;
        
        let spellActionBack = SKAction.moveToX(userPosX, duration: 0.02);
        spellActionBack.timingMode = SKActionTimingMode.EaseOut;
        
        //Show Spell Particle
        sender.spellEmmiter.removeFromParent();
        let spellEmmitterPath:NSString = NSBundle.mainBundle().pathForResource("spell", ofType: "sks")!
        sender.spellEmmiter = NSKeyedUnarchiver.unarchiveObjectWithFile(spellEmmitterPath) as SKEmitterNode
        sender.spellEmmiter?.position = CGPoint(x:0, y:0);
        sender.spellEmmiter.zPosition = 5;
        sender.addChild(sender.spellEmmiter);
        
        //Spell Emitter Animations
        let spellEmitterFadeIn = SKAction.fadeInWithDuration(0.4);
        let spellEmitterSendToOppnent = SKAction.moveToX(sender.frame.width + 300, duration: 1.0);
        spellEmitterSendToOppnent.timingMode = SKActionTimingMode.Linear;
        let spellEmitterReturnToOriginalPos = SKAction.moveToX(0, duration: 0);
        let spellEmitterFadeOut = SKAction.fadeOutWithDuration(1.0);
        let spellEmitterWaitToReturn = SKAction.waitForDuration(2.0);
        
        //Add sequence
        let spellEmitterSequence = SKAction.sequence(
            [
                spellEmitterFadeIn,
                spellEmitterSendToOppnent,
                spellEmitterWaitToReturn,
                spellEmitterFadeOut,
                spellEmitterReturnToOriginalPos
            ]);
        
        sender.spellEmmiter.hidden = false;
        sender.spellEmmiter.runAction(spellEmitterSequence);
        
        let waitAction = SKAction.waitForDuration(2.0);
        let attackSequence = SKAction.sequence([spellActionFront, spellActionBack,waitAction]);
        
        //self.currState = States.idle;
        sender.runAction(attackSequence, completion: sender.idleState);    }
    
    override func applyReceiver(receiver:Lumbot){
        super.applyReceiver(receiver);
        
        
        let userPosX = receiver.position.x;
        
        //Spell Animation
        let spellActionFront = SKAction.moveToX(userPosX + 20.0, duration: 0.5);
        spellActionFront.timingMode = SKActionTimingMode.EaseOut;
        let spellActionBack = SKAction.moveToX(userPosX, duration: 1.0);
        spellActionBack.timingMode = SKActionTimingMode.EaseOut;
        let waitAction = SKAction.waitForDuration(1.0);
        let attackSequence = SKAction.sequence([waitAction, spellActionFront, spellActionBack]);
        
        //Show Spell Particle
        receiver.spellEmmiter.removeFromParent();
        let spellEmmitterPath:NSString = NSBundle.mainBundle().pathForResource("spell", ofType: "sks")!
        receiver.spellEmmiter = NSKeyedUnarchiver.unarchiveObjectWithFile(spellEmmitterPath) as SKEmitterNode
        receiver.spellEmmiter?.position = CGPoint(x:-receiver.frame.width*2, y:0);
        receiver.spellEmmiter.zPosition = 2;
        receiver.addChild(receiver.spellEmmiter);
        
        //Spell Emitter Animations
        let spellEmitterFadeIn = SKAction.fadeInWithDuration(0.4);
        let spellEmitterReceiveToOppnent = SKAction.moveToX(0, duration: 1.0);
        spellEmitterReceiveToOppnent.timingMode = SKActionTimingMode.Linear;
        let spellEmitterFadeOut = SKAction.fadeOutWithDuration(1.0);
        
        //Add sequence
        let spellEmitterSequence = SKAction.sequence(
            [
                spellEmitterFadeIn,
                spellEmitterReceiveToOppnent,
                spellEmitterFadeOut
            ]);
        receiver.spellEmmiter.hidden = false;
        receiver.spellEmmiter.runAction(spellEmitterSequence);
        
        
        //Damage Spell
        let dmgShow = SKAction.fadeInWithDuration(0.2);
        let dmgWait = SKAction.waitForDuration(0.8);
        let dmgHide = SKAction.fadeOutWithDuration(0.2);
        let dmgSequence = SKAction.sequence([waitAction,dmgShow,dmgWait, dmgHide]);
        receiver.damageEmmiter.runAction(dmgSequence);
        receiver.damageEmmiter.hidden = false;
        
        //change state
        //self.currState = States.idle;
        receiver.runAction(attackSequence, completion: receiver.idleState);
    }
}