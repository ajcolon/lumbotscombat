//
//  ClashAbility.swift
//  Lumbots
//
//  Created by Albith Joel Colon Figueroa on 1/3/15.
//  Copyright (c) 2015 Albith Joel Colon. All rights reserved.
//

import Foundation
import SpriteKit

class ClashAbility:Ability{
    
    override init(type: AbilityManager.AbilityType, ammount: Int, energy: Int) {
        super.init(type: type, ammount: ammount, energy: energy);
        self.imagePath = "abuttonClash";
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func applySender(sender:Lumbot){
        super.applySender(sender);
        
        //Affect User Health
        var amountToDeduct = self.ammount!/2;
        sender.setHealth(-amountToDeduct);
        
        //Set Animation
        let userPosX = sender.position.x;
        let clashActionFront = SKAction.moveToX(userPosX + 200.0, duration: 0.5);
        clashActionFront.timingMode = SKActionTimingMode.EaseOut;
        
        let clashActionBack = SKAction.moveToX(userPosX, duration: 1.0);
        clashActionBack.timingMode = SKActionTimingMode.EaseOut;
        
        let clashSequence = SKAction.sequence([clashActionFront,clashActionBack]);
        
        //sender.currState = States.idle;
        sender.runAction(clashSequence, completion: sender.idleState);
    }
    
    override func applyReceiver(receiver:Lumbot){
        super.applyReceiver(receiver);
        
        let waitAction = SKAction.waitForDuration(0.8);
        
        let userPosX = receiver.position.x;
        let clashActionFront = SKAction.moveToX(userPosX + 200.0, duration: 0.5);
        clashActionFront.timingMode = SKActionTimingMode.EaseOut;
        
        let clashActionBack = SKAction.moveToX(userPosX, duration: 1.0);
        clashActionBack.timingMode = SKActionTimingMode.EaseOut;
        
        let clashSequence = SKAction.sequence([waitAction, clashActionFront,clashActionBack]);
        
        //self.currState = States.idle;
        receiver.runAction(clashSequence, completion: receiver.idleState);
    }
}