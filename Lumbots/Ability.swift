//
//  Ability.swift
//  Lumbots
//
//  Created by Albith Joel Colon on 12/10/14.
//  Copyright (c) 2014 Albith Joel Colon. All rights reserved.
//

import Foundation

protocol Executable{
    func applySender(receiver: Lumbot)
    func applyReceiver(receiver: Lumbot)
}

class Ability:NSObject,Executable{
    
    var type:AbilityManager.AbilityType?;
    var ammount:Int?;
    var reqEnergy:Int?;
    var imagePath:String = "abuttonAttack";
    var senderAnimation:String?;
    var responderAnimation:String?;
    var affectsReceiversHealth:Bool?=true;
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(type: AbilityManager.AbilityType, ammount:Int, energy:Int){
        super.init();
        self.type = type;
        self.ammount = ammount;
        self.reqEnergy = energy;
    }
    
    func applySender(sender:Lumbot){
        println("handleSender");
        sender.energy = sender.energy - self.reqEnergy!;
    }
    
    func applyReceiver(receiver:Lumbot){
        println("handleReceiver");
        
        if(self.affectsReceiversHealth == true){
            receiver.setHealth(-self.ammount!);
            receiver.showHitPoints(-self.ammount!);
        }
    }
    
    func objectRepresentation()->NSDictionary{
        let object:NSDictionary =
        [
            "type":self.type!.rawValue,
            "ammount":self.ammount!,
            "reqEnergy":self.reqEnergy!,
            "imagePath": self.imagePath,
            "affectsReceiversHealth":self.affectsReceiversHealth!
        ];
        return object;
    }
}