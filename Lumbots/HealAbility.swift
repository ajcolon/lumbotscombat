//
//  HealAbility.swift
//  Lumbots
//
//  Created by Albith Joel Colon Figueroa on 1/3/15.
//  Copyright (c) 2015 Albith Joel Colon. All rights reserved.
//

import Foundation
import SpriteKit

class HealAbility:Ability{
    
    override init(type: AbilityManager.AbilityType, ammount: Int, energy: Int) {
        super.init(type: type, ammount: ammount, energy: energy);
        self.imagePath = "abuttonHeal";
        self.affectsReceiversHealth = false;
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func applySender(sender:Lumbot){
        super.applySender(sender);
        
        //Affect User Health
        sender.setHealth(self.ammount!);
        
        //Lumbot Animations
        let userPosY = sender.position.y;
        let healActionFront = SKAction.moveToY(userPosY + 40.0, duration: 0.5);
        healActionFront.timingMode = SKActionTimingMode.EaseOut;
        let healActionBack = SKAction.moveToY(userPosY, duration: 1.0);
        healActionFront.timingMode = SKActionTimingMode.EaseOut;
        
        //Add Spell Emitter
        sender.spellEmmiter.removeFromParent();
        let spellEmmitterPath:NSString = NSBundle.mainBundle().pathForResource("healing", ofType: "sks")!
        sender.spellEmmiter = NSKeyedUnarchiver.unarchiveObjectWithFile(spellEmmitterPath) as SKEmitterNode
        sender.spellEmmiter?.position = CGPoint(x:0, y:0);
        sender.spellEmmiter.zPosition = 5;
        sender.addChild(sender.spellEmmiter);
        
        //emitter animations
        let spellEmitterFadeIn = SKAction.fadeInWithDuration(0.4);
        let spellEmitterWaitToReturn = SKAction.waitForDuration(2.0);
        let spellEmitterFadeOut = SKAction.fadeOutWithDuration(1.0);
        
        //animaions sequence
        let spellEmitterSequence = SKAction.sequence(
            [
                spellEmitterFadeIn,
                spellEmitterWaitToReturn,
                spellEmitterFadeOut
            ]);
        
        sender.spellEmmiter.hidden = false;
        sender.spellEmmiter.runAction(spellEmitterSequence);
        
        let attackSequence = SKAction.sequence([healActionFront, healActionBack]);
        
        //sender.currState = States.idle;
        sender.runAction(attackSequence, completion: sender.idleState);
    }
    
    override func applyReceiver(receiver:Lumbot){
        super.applyReceiver(receiver);
        
        let userPosX = receiver.position.x;
        let healActionFront = SKAction.moveToX(userPosX + 200.0, duration: 0.5);
        healActionFront.timingMode = SKActionTimingMode.EaseOut;
        
        let healActionBack = SKAction.moveToX(userPosX, duration: 1.0);
        healActionBack.timingMode = SKActionTimingMode.EaseOut;
        
        
        let waitAction = SKAction.waitForDuration(0.8);
        let attackSequence = SKAction.sequence([waitAction, healActionFront, healActionBack]);
        
        //receiver.currState = States.idle;
        receiver.runAction(attackSequence, completion: receiver.idleState);
    }
}