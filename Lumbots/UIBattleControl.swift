//
//  UIBattleControl.swift
//  Lumbots
//
//  Created by Albith Joel Colon on 1/7/15.
//  Copyright (c) 2015 Albith Joel Colon. All rights reserved.
//

import Foundation
import SpriteKit;

class UIBattleControl:SKSpriteNode{
    
    let bgImageName = "BattleControl";
    let dotImageName = "energyDot";
    let healthBar = SKSpriteNode(imageNamed: "HealthBar");
    let energyDot = SKSpriteNode(imageNamed: "EnergyDots");
    var turnButton = SKSpriteNode(imageNamed: "TurnButton-End");
    var energyDots = [SKSpriteNode]();
    let maxEnergyAmount = 5;

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override init(){
        let imageTexture = SKTexture(imageNamed: self.bgImageName);
        super.init(texture:imageTexture, color:nil, size: imageTexture.size());
        self.loadUI();
    }
    
    func loadUI(){
    
        //add health bar
        self.healthBar.xScale = 1;
        self.healthBar.yScale = 1;
        //self.healthBar.size = CGSizeMake(100 , self.healthBar.size.height);
        self.healthBar.anchorPoint = CGPointMake(0.0, 0.5);
        self.healthBar.position = CGPoint(x:CGRectGetMinX(self.frame)+10, y:CGRectGetMidY(self.frame));
        self.addChild(self.healthBar);
        self.hide();
        
        
        //Load EnergyDots
        self.energyDot.position = CGPoint(x:CGRectGetMidX(self.frame)-100, y:CGRectGetMidY(self.frame)+30);
        for (var i = 0 ; i < self.maxEnergyAmount ; i++){
            var newDot = energyDot.copy() as SKSpriteNode;
            let paddingFromLeft = CGFloat(50);
            let spaceFromPrev = CGFloat(i);
            var newDotXPos = paddingFromLeft * spaceFromPrev ;
            newDot.position = CGPoint(x:newDot.position.x + newDotXPos, y:newDot.position.y);
            self.addChild(newDot);

            self.energyDots.append(newDot);
        }
        
        //Add Turn Button
        self.turnButton.xScale = 1;
        self.turnButton.name = "turnButton";
        self.turnButton.yScale = 1;
        self.turnButton.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame)-24);
        self.addChild(self.turnButton);
        
    }
    
    func updateDisplay(lumbot: Lumbot){
        let lumbotHealth = CGFloat(lumbot.getHealth())
        let lumbotHealthFloat = lumbotHealth/100;
        if(lumbotHealthFloat >= 0){
            self.healthBar.xScale = lumbotHealth/100;
        }
        
        let lumbotEnergy = lumbot.energy;
        for(var i = 0 ; i < self.energyDots.count ; i++){
            if(lumbotEnergy > i){
                self.energyDots[i].texture = SKTexture(imageNamed: "EnergyDots");
            }else{
                self.energyDots[i].texture = SKTexture(imageNamed: "EnergyDotsEmpty");
            }
        }
        
    }
    
    func displayMyTurn(){
        self.turnButton.texture = SKTexture(imageNamed: "TurnButton-End");
    }
    
    func displayOpponentTurn(){
        self.turnButton.texture = SKTexture(imageNamed: "TurnButton-Opponent");
    }
    
    func show(){
        self.hidden = false;
    }
    
    func hide(){
        self.hidden = true;
    }
    
    
}