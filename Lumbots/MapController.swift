//
//  MapController.swift
//  Lumbots
//
//  Created by Albith Joel Colon on 12/8/14.
//  Copyright (c) 2014 Albith Joel Colon. All rights reserved.
//

import Foundation
import UIKit;
import MapKit;

class MapController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate {
    
    
    @IBAction func closeDialog(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    @IBOutlet weak var mapView: MKMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        //Create Location
        let location = CLLocationCoordinate2D(
            latitude: 37.2521,
            longitude: -121.8615
        )
        
        // Define Spanning of the region
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        self.mapView.setRegion(region, animated: true)
        
        //Addd Anotations
        let annotation = MKPointAnnotation()
        annotation.setCoordinate(location)
        annotation.title = "Big Ben"
        annotation.subtitle = "London"
        self.mapView.addAnnotation(annotation)
        
        
    }
    
    func mapView (mapView: MKMapView!,
        viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
            
            var pinView:MKPinAnnotationView = MKPinAnnotationView()
            pinView.annotation = annotation
            pinView.pinColor = MKPinAnnotationColor.Red
            pinView.animatesDrop = true
            pinView.draggable = true
            pinView.canShowCallout = true
            
            return pinView
    }
    
    //MOVE Annotation
    
//    func shiftToCorner{
//        //ignore "Annotation", this is just my own custom MKAnnotation subclass
//        Annotation *currentAnnotation = [[mapView selectedAnnotations] objectAtIndex:0];
//        [mapView setCenterCoordinate:currentAnnotation.coordinate];
//        
//        CGPoint fakecenter = CGPointMake(20, 20);
//        CLLocationCoordinate2D coordinate = [mapView convertPoint:fakecenter toCoordinateFromView:mapView];
//        [mapView setCenterCoordinate:coordinate animated:YES];
//    }
    
    func mapView(mapView: MKMapView!,
        didSelectAnnotationView view: MKAnnotationView!){
            
            println("Selected annotation")
    }
    
    override func didReceiveMemoryWarning() {
        //
    }
}