//
//  RayAbility.swift
//  Lumbots
//
//  Created by Albith Joel Colon Figueroa on 1/3/15.
//  Copyright (c) 2015 Albith Joel Colon. All rights reserved.
//

import Foundation
import SpriteKit

class RayAbility:Ability{
    
    override func applySender(sender:Lumbot){
        super.applySender(sender);
        let userPosX = sender.position.x;
        let clashActionFront = SKAction.moveToX(userPosX + 200.0, duration: 0.5);
        clashActionFront.timingMode = SKActionTimingMode.EaseOut;
        
        let clashActionBack = SKAction.moveToX(userPosX, duration: 1.0);
        clashActionBack.timingMode = SKActionTimingMode.EaseOut;
        
        let clashSequence = SKAction.sequence([clashActionFront,clashActionBack]);
        
        //sender.currState = States.idle;
        sender.runAction(clashSequence, completion: sender.idleState);
    }
    
    override func applyReceiver(receiver:Lumbot){
        super.applyReceiver(receiver);
        
        let waitAction = SKAction.waitForDuration(0.8);
        
        let userPosX = receiver.position.x;
        let clashActionFront = SKAction.moveToX(userPosX + 200.0, duration: 0.5);
        clashActionFront.timingMode = SKActionTimingMode.EaseOut;
        
        let clashActionBack = SKAction.moveToX(userPosX, duration: 1.0);
        clashActionBack.timingMode = SKActionTimingMode.EaseOut;
        
        let clashSequence = SKAction.sequence([waitAction, clashActionFront,clashActionBack]);
        
        //self.currState = States.idle;
        receiver.runAction(clashSequence, completion: receiver.idleState);
    }
}