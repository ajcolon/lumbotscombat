//
//  HelmetCell.swift
//  Lumbots
//
//  Created by Albith Joel Colon on 12/7/14.
//  Copyright (c) 2014 Albith Joel Colon. All rights reserved.
//

import Foundation
import UIKit;

class HelmetTableViewCell: UITableViewCell {
    @IBOutlet weak var helmetImage: UIImageView!
    @IBOutlet weak var ability1Image: UIImageView!
    @IBOutlet weak var ability1Energy: UILabel!
    @IBOutlet weak var ability1Amount: UILabel!
    @IBOutlet weak var ability2Image: UIImageView!
    @IBOutlet weak var ability2Energy: UILabel!
    @IBOutlet weak var ability2Amount: UILabel!
    @IBOutlet weak var ability3Image: UIImageView!
    @IBOutlet weak var ability3Energy: UILabel!
    @IBOutlet weak var ability3Amount: UILabel!
    @IBOutlet weak var ability1AttackIcon: UIImageView!
    @IBOutlet weak var ability1EnergyIcon: UIImageView!
    @IBOutlet weak var ability2AttackIcon: UIImageView!
    @IBOutlet weak var ability2EnergyIcon: UIImageView!
    @IBOutlet weak var ability3AttackIcon: UIImageView!
    @IBOutlet weak var ability3EnergyIcon: UIImageView!
}
