//
//  Lumbot.swift
//  Lumbots
//
//  Created by Albith Joel Colon on 12/4/14.
//  Copyright (c) 2014 Albith Joel Colon. All rights reserved.
//

import Foundation
import SpriteKit


class Lumbot:SKSpriteNode{
    
    var onGround = true;
    var velocityY = CGFloat(0);
    var lumbotBaseline = CGFloat(0);
    var helmet:Helmet?;
    
    let gravity = CGFloat(0.9);
    let rotationSpeed = 5;
    var floatEmmiter:SKEmitterNode!;
    var spellEmmiter:SKEmitterNode!;
    var damageEmmiter:SKEmitterNode!;
    var hitpointLbl = SKLabelNode(fontNamed:"Futura-Medium");
    
    //wobble properties
    var angle = 0.0;
    var range = 10.0
    var lumbotWobbleSpeed = 0.03;
    var actions = Array<SKAction>();
    
    //attributes
    var health:Int!;
    var energy:Int!;
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(imageNamed: String){
        self.energy = 0;
        self.health = 100;
        let imageTexture = SKTexture(imageNamed: imageNamed)
        super.init(texture:imageTexture, color:nil, size: imageTexture.size());
        self.setUIElements();
        //self.idleState();
    }

    func setUIElements(){
        //Add Floating Emitter
        let sparkEmmitterPath:NSString = NSBundle.mainBundle().pathForResource("fire", ofType: "sks")!
        self.floatEmmiter = NSKeyedUnarchiver.unarchiveObjectWithFile(sparkEmmitterPath) as SKEmitterNode
        self.floatEmmiter?.position = CGPoint(x:0, y:CGRectGetMinY(self.frame)+110);
        self.floatEmmiter?.name = "sparkEmmitter";
        self.floatEmmiter?.zPosition = 2;
        
        //Add Spell Emitter
        let spellEmmitterPath:NSString = NSBundle.mainBundle().pathForResource("healing", ofType: "sks")!
        self.spellEmmiter = NSKeyedUnarchiver.unarchiveObjectWithFile(spellEmmitterPath) as SKEmitterNode
        self.spellEmmiter?.position = CGPoint(x:0, y:CGRectGetMaxY(self.frame));
        self.spellEmmiter?.name = "sparkEmmitter";
        self.spellEmmiter?.zPosition = 5;
        self.addChild(spellEmmiter);
        self.spellEmmiter.hidden = true;
        
        //Add Damage Emitter
        let damageEmmitterPath:NSString = NSBundle.mainBundle().pathForResource("damageSpark", ofType: "sks")!
        self.damageEmmiter = NSKeyedUnarchiver.unarchiveObjectWithFile(damageEmmitterPath) as SKEmitterNode
        self.damageEmmiter?.position = CGPoint(x:CGRectGetMinX(self.frame), y:CGRectGetMidY(self.frame));
        self.damageEmmiter?.name = "sparkEmmitter";
        self.damageEmmiter?.zPosition = 5;
        self.addChild(damageEmmiter);
        //self.damageEmmiter.hidden = true;
        damageEmmiter.runAction(SKAction.fadeOutWithDuration(0));
        
        //Render Helmet
        self.helmet = Helmet(imageNamed: "00007");
        self.helmet?.xScale = 1;
        self.helmet?.yScale = 1;
        self.helmet?.zPosition = 3;
        self.helmet?.position = CGPoint(x:0, y:0);
        self.addChild(self.helmet!)
        
        var mask = SKSpriteNode(imageNamed: "LumbotFireMask");
        //var mask = self;
        var cropNode = SKCropNode();
        cropNode.zPosition = 4;
        cropNode.position = CGPoint(x:0, y:0);
        cropNode.addChild(self.floatEmmiter!);
        cropNode.maskNode = mask;
        self.addChild(cropNode);
        
        //Add hitpoint label
        self.hitpointLbl = SKLabelNode(fontNamed:"Futura-Medium");
        self.hitpointLbl.text = "-200";
        self.hitpointLbl.color = UIColor.yellowColor();
        self.hitpointLbl.zPosition = 99;
        self.hitpointLbl.hidden = true;
        self.hitpointLbl.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame));
        self.addChild(self.hitpointLbl);
    }
    
    func random()->UInt32{
        var range = UInt32(50)...UInt32(200)
        return range.startIndex + arc4random_uniform(range.endIndex - range.startIndex + 1)
    }
    
    func setHealth(health:Int){
        self.health = self.health + health;
        
        if(self.health < 0){
            self.health = 0;
        }
    }
    
    func showHitPoints(hitpoint : Int){
        self.hitpointLbl.hidden = false;
        let originalPosition = CGPointMake(self.hitpointLbl.position.x,self.hitpointLbl.position.y);
        let hitpointShow = SKAction.fadeInWithDuration(0.2);
        let hitpointMove = SKAction.moveTo(CGPoint(x:self.hitpointLbl.position.x, y:self.hitpointLbl.position.y+400), duration: 0.7);
        let hitpointHide = SKAction.fadeOutWithDuration(0.2);
        let hitpointBack = SKAction.moveTo(originalPosition, duration: 0.0);
        let hitpointSequence = SKAction.sequence([hitpointShow,hitpointMove,hitpointHide,hitpointBack]);
        self.hitpointLbl.runAction(hitpointSequence);
    }
    
    func getHealth()->Int{
        return self.health!;
    }
    
    func hasEnoughEnergy(reqEnergy:Int)->Bool{
        if(self.energy >= reqEnergy){
            return true;
        }else{
            println("Lumbot doesnt have enough energy");
            return false;
        }
    }
    
    func idleState(){
        let idleUpAction = SKAction.moveToY(self.position.y + 20.0, duration: 1.8);
        idleUpAction.timingMode = SKActionTimingMode.EaseOut;
        
        let idleDownAction = SKAction.moveToY(self.position.y - 20.0, duration: 1.8);
        idleDownAction.timingMode = SKActionTimingMode.EaseIn;
        
        let idleSequence = SKAction.sequence([idleUpAction,idleDownAction]);
        let idleForever = SKAction.repeatActionForever(idleSequence);
        self.runAction(idleForever);
    }
    
    func handleEnergyTransaction(energy:Int){
        self.energy = self.energy - energy;
    }
    
    func handleAbility(ability: Ability){
        
        self.handleEnergyTransaction(ability.reqEnergy!);
        ability.applySender(self);
    }
    
    func setHelmet(helmet:Helmet){
        println("setHelmet");
        
        self.helmet?.changeImage(helmet.imagePath);
        self.helmet?.xScale = 1;
        self.helmet?.abilities = helmet.abilities;
        self.helmet?.yScale = 1;
        self.helmet?.zPosition = 3;
        self.helmet?.position = CGPoint(x:0, y:0);
    }
    
    func update(currentTime: CFTimeInterval){
        
        //TODO: UPDATE FRAMES FOR EYES??
    }
    
    
}